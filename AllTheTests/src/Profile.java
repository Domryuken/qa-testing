
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Profile {

	WebDriver driver;
	
	@FindBy(xpath = "/html/body/nav[1]/div/div/div/ul/li[4]/a")
	WebElement tours;
	
	@FindBy(xpath = "/html/body/nav[1]/div/div/div/ul/li[8]/a")
	WebElement contact;
	
	@FindBy(xpath = "/html/body/div[3]/div[3]/div/div[1]/ul/li[2]/a")
	WebElement profile;
	
	public Profile(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getTours(){
		return tours;
	}
	
	public WebElement getContact(){
		return contact;
	}
	
	public WebElement getProfile(){
		return profile;
	}
	
	
}