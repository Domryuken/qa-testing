
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import org.testng.annotations.Test;

import com.google.common.base.Function;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;




public class AllTheTests2 {
	
	WebDriver driver;
	Wait<WebDriver> wait;
	Home home;
	Login login;
	Profile profile;
	ExtentReports report;
	
	
	@BeforeClass
	public void start(){
		report = new ExtentReports("C:\\Users\\Administrator\\Downloads\\report.html",true);
	}
	
	@AfterClass
	public void end(){
		report.flush();
	}
	
	
//	resets things used between all tests
	@BeforeMethod
	public void setup(){

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Administrator\\Desktop\\selenium\\SeleniumFiles\\Selenium\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);
		driver.get("http://www.phptravels.net");
		wait = new FluentWait<WebDriver>(driver).withTimeout(30, TimeUnit.SECONDS)
				.pollingEvery(1, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		home = new Home(driver);
		login = new Login(driver);
		profile = new Profile(driver);
		
	}
	
	
	
	
	
//	Logs in user, navigates to tours page, checks 3 different products and purchases the third one
	@Test
	public void test1() throws InterruptedException {
		String email = "user@phptravels.com";
		String password = "demouser";
		ExtentTest test = report.startTest("Verify log in and book");

		boolean result = true;
		try {
			
			// get to log in page
			home.getDropDown().click();	
			home.getLogin().click();
			test.log(LogStatus.INFO, "Navigated to log in page");
			
			// log in
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					if(login.getUsername().isDisplayed()){	
						return login.getUsername();
					}
					return null;
				}
			});
			login.getUsername().sendKeys(email);
			login.getPassword().sendKeys(password);
			login.getLogin().click();
			test.log(LogStatus.INFO, "Logged in");
			
			// move to tours page
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(By.xpath("/html/body/div[3]/div[3]/div/div[1]/ul/li[2]/a"));
				}
			});
			profile.getTours().click();
			test.log(LogStatus.INFO, "Navigated to Tours page");
			
			
			// check 3 different tour
			((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
			driver.findElement(
					By.xpath("/html/body/div[5]/div[3]/div/table/tbody/tr[1]/td/div/div[2]/div/div[1]/a/button"))
					.click();
			driver.navigate().back();
			((JavascriptExecutor) driver).executeScript("scroll(0,400)");
			driver.findElement(
					By.xpath("/html/body/div[5]/div[3]/div/table/tbody/tr[2]/td/div/div[2]/div/div[1]/a/button"))
					.click();
			driver.navigate().back();
			((JavascriptExecutor) driver).executeScript("scroll(0,400)");
			driver.findElement(
					By.xpath("/html/body/div[5]/div[3]/div/table/tbody/tr[3]/td/div/div[2]/div/div[1]/a/button"))
					.click();
			test.log(LogStatus.INFO, "Checked 3 tours");

			// book last tour checked
			((JavascriptExecutor) driver).executeScript("scroll(0,2000)");
			driver.findElement(By.xpath("//*[@id=\"OVERVIEW\"]/div/div[2]/div[2]/div[2]/div/form/div[4]/button"))
					.click();
			driver.findElement(By.xpath("/html/body/div[3]/div/div/div[1]/div/div[2]/div[5]/button")).click();
			new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".arrivalpay"))).click();
			driver.switchTo().alert().accept();
			test.log(LogStatus.INFO, "Reserved a tour");
			
			//check if reserved
			if(driver.findElement(By.cssSelector(".btn-success")).equals(null)){
				result = false;
			}
			
			
		} catch (NoSuchElementException e) {
			test.log(LogStatus.FAIL, "Element was not there");
			System.out.println(e);
			result = false;
		}

		finally {
			driver.quit();
		}

		if (result)
			test.log(LogStatus.PASS, "Complete");
		else
			test.log(LogStatus.FAIL, "Unexpected end result");
		
		report.endTest(test);
	}

	
	
//	Logs in, navigates to details page, changes the address and logs back out
	@Test
	public void test2() {
		String email = "user@phptravels.com";
		String password = "demouser";
		ExtentTest test = report.startTest("Verify Change account details");
		boolean result = true;
		try {

			//go to log in page
			home.getDropDown().click();
			home.getLogin().click();
			
			test.log(LogStatus.INFO, "Navigated to log in page");
			
			//logs in
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					if(login.getUsername().isDisplayed()){	
						return login.getUsername();
					}
					return null;
				}
			});
			login.getUsername().sendKeys(email);
			login.getPassword().sendKeys(password);
			login.getLogin().click();
			test.log(LogStatus.INFO, "Logged in");
			
			//moves to profile page
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(By.xpath("/html/body/div[3]/div[3]/div/div[1]/ul/li[2]/a"));
				}
			});
			profile.getProfile().click();
			test.log(LogStatus.INFO, "Navigated to profile tab");

			
			//changes the address and commits the changes
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					while (true) {
						if (driver.findElement(By.name("address1")).isDisplayed())
							return driver.findElement(By.name("address1"));
					}
				}
			});
			driver.findElement(By.name("address1")).sendKeys("Ruining the address field in this form");
			driver.findElement(By.cssSelector(".updateprofile")).click();
			test.log(LogStatus.INFO, "updated address");


			//logs back out
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/ul/li[2]/a")).click();
			driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/ul/li[2]/ul/li[2]/a")).click();

			test.log(LogStatus.INFO, "Log back out");
		} catch (Exception e) {
			System.out.println(e);
			result = false;
		}finally {
			driver.quit();
		}

		if (result)
			System.out.println("---Passed---");
		else
			System.out.println("---Failed---");
	}

	
	
//	navigates to sign up page, creates a new user, logs out of the new account and logs back in with the new details
	@Test
	public void test3() {

		boolean result = true;
		ExtentTest test = report.startTest("Verify create account, sign out and sign back in");
		try {
			
			//go to log in page
			home.getDropDown().click();
			home.getSigup().click();
			test.log(LogStatus.INFO, "Navigated to sign up page");
			
			//log in
			driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/ul/li[2]/a")).click();
			driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/ul/li[2]/ul/li[2]/a")).click();
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					if(driver.findElement(By.name("firstname")).isDisplayed()){	
						return driver.findElement(By.name("firstname"));
					}
					return null;
				}
			});
			driver.findElement(By.name("firstname")).sendKeys("something");
			driver.findElement(By.name("lastname")).sendKeys("something");
			driver.findElement(By.name("phone")).sendKeys("07777777777");

			
			//create a new account with a semi random email
			Random rand = new Random();
			int n = rand.nextInt(100000000);
			String email = "something" + n + "@something.com";
			driver.findElement(By.name("email")).sendKeys(email);

			driver.findElement(By.name("password")).sendKeys("something");
			driver.findElement(By.name("confirmpassword")).sendKeys("something");
			driver.findElement(By.cssSelector(".signupbtn")).click();
			test.log(LogStatus.INFO, "Create new account");

			//log back out
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(By.xpath("/html/body/div[3]/div[1]/div/div[1]/h3"));
				}
			});
			home.getDropDown().click();
			home.getSigup().click();
			test.log(LogStatus.INFO, "Logged back out");
			
			//log back in
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {								
					if(login.getUsername().isDisplayed()){	
						return login.getUsername();
					}
					return null;
				}
			});
			login.getUsername().sendKeys(email);
			login.getPassword().sendKeys("something");
			login.getLogin().click();

			test.log(LogStatus.INFO, "Logged back in with new account");
			
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Element not present");
			result = false;
		}finally {
			 driver.quit();
		}

		if (result)
			test.log(LogStatus.PASS, "Test Complete");
		else
			test.log(LogStatus.FAIL, "Unexpected outcome");
	}
	
	
//	logs into account, navigates to the contact us page and writes a new review and sends it.
	@Test
	public void test4() {

		boolean result = true;
		ExtentTest test = report.startTest("Verify log in and submit a review");
		try {
			String email = "user@phptravels.com";
			String password = "demouser";
			
			
			//go to log in page
			home.getDropDown().click();
			home.getLogin().click();
			test.log(LogStatus.INFO, "Navigated to sign up page");

			//log in
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {								
					if(login.getUsername().isDisplayed()){	
						return login.getUsername();
					}
					return null;
				}
			});
			login.getUsername().sendKeys(email);
			login.getPassword().sendKeys(password);
			login.getLogin().click();
			test.log(LogStatus.INFO, "logged in");

			
			//go to contact page
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(By.xpath("/html/body/div[3]/div[3]/div/div[1]/ul/li[2]/a"));
				}
			});
			profile.getContact().click();
			test.log(LogStatus.INFO, "Navigated to contact page");

			//write and post review
			wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(By.xpath("/html/body/div[3]/div/div/div/form/div[1]/div[2]/input[1]"));
				}
			});
			driver.findElement(By.xpath("/html/body/div[3]/div/div/div/form/div[1]/div[2]/input[1]")).sendKeys("NAME");
			driver.findElement(By.xpath("/html/body/div[3]/div/div/div/form/div[1]/div[2]/input[2]")).sendKeys("email@email.com");
			driver.findElement(By.xpath("/html/body/div[3]/div/div/div/form/div[1]/div[2]/input[3]")).sendKeys("SUBJECT");
			driver.findElement(By.xpath("/html/body/div[3]/div/div/div/form/div[1]/div[3]/textarea")).sendKeys("Website doesnt work unless window is maximised. 8/10");
			driver.findElement(By.xpath("/html/body/div[3]/div/div/div/form/div[2]/input")).click();
			test.log(LogStatus.INFO, "filled out contact form and submitted");
			if(driver.findElement(By.cssSelector(".alert-success")).equals(null)){
				result = false;
			}

			
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Element not found");
			result = false;
		}

		finally {
			 driver.quit();
		}

		if (result)
			test.log(LogStatus.PASS, "Test complete");
		else
			test.log(LogStatus.FAIL, "Unexpected end result");

	}
	
	
	
	
}
