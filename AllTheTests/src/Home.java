
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Home {

	WebDriver driver;
	
	@FindBy(xpath = "/html/body/div[2]/div/div/div[2]/ul/li[2]/a")
	WebElement dropDown;
	
	@FindBy(xpath = "/html/body/div[2]/div/div/div[2]/ul/li[2]/ul/li[1]/a")
	WebElement login;
	
	@FindBy(xpath = "/html/body/div[2]/div/div/div[2]/ul/li[2]/ul/li[2]/a")
	WebElement signup;
	
	public Home(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getDropDown(){
		return dropDown;
	}
	
	public WebElement getLogin(){
		return login;
	}
	
	public WebElement getSigup(){
		return signup;
	}
	
}
